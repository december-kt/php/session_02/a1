<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="widtd=device-width initial-scale=1">

		<title>Repetition Control Structures and Array Manipulation</title>
	</head>
	<body>

		<h1>Divisibles of Five</h1>

		<?php divisibleByFive(); ?>

		<h1>Array Manipulation</h1>
		
		<?php array_push($students, 'John Smith'); ?>

		<?php var_dump($students); ?>

		<br/><br/>
		
		<?php echo count($students); ?>

		<br/><br/>

		<?php array_push($students, 'Jane Smith'); ?>

		<?php var_dump($students); ?>

		<br/><br/>

		<?php echo count($students); ?>

		<br/><br/>

		<?php array_pop($students); ?>

		<?php var_dump($students); ?>

		<br/><br/>

		<?php echo count($students); ?>
	</body>
</html>